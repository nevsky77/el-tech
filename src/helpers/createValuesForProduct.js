const createValues = function (values) {
  return  values.map((el, i) => {
    let valueObject = {
      value: '',
      title: '',
      value_id: 0
    }
    valueObject.value = el
    valueObject.title = `${el}`
    valueObject.value_id = ++i
   return valueObject
  })
}
export default createValues