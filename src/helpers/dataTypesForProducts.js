export const SHUKNSdataModel = {
  id: {
    name: 'id',
    dataType: 'integer'
  },
  user_id: {
    name: 'user_id',
    dataType: 'integer'
  },
  type: {
    name: 'type',
    dataType: 'integer'
  },
  name: {
    name: 'name',
    dataType: 'string'
  },
  save_json: {
    name: 'save_json',
    dataType: 'object',
    values: {
      power: {
        name: 'power',
        dataType: 'integer'
      },
      pumpsCount: {
        name: 'pumpsCount',
        dataType: 'integer'
      },
      starter: {
        name: 'starter',
        dataType: 'string'
      },
      inputCount: {
        name: 'inputCount',
        dataType: 'integer'
      },
      ip: {
        name: 'ip',
        dataType: 'string'
      },
      zdv: {
        name: 'zdv',
        dataType: 'integer'
      },
      producer: {
        name: 'producer',
        dataType: 'string'
      },
      gsm: {
        name: 'gsm',
        dataType: 'string'
      }
    }
  },

}