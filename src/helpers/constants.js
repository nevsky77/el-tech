export const ProductTypes = {
  shukns: 'shukns',
}

export const ElementTypes = {
  input: 'input',
  select: 'select',
  checkbox: 'checkbox',
  dropdown: 'dropdown',
  switch: 'switch'
}
export const DataTypes = {
  array: 'array',
  string: 'string',
  integer: 'integer',
  boolean: 'boolean',
  object: 'object'
}