import Vue from 'vue'
import vuetify from './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from '../store'
import Vuelidate from 'vuelidate'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.axios.defaults.baseURL = 'http://188.227.120.13:8080/';
export const eventBus = new Vue();


Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(Vuelidate)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
