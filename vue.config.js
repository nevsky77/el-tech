module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? './'
    : '/',
  chainWebpack: config => {
    config.module.rules.delete('eslint');
  }
}

// module.exports = {
//   publicPath: process.env.NODE_ENV === 'production'
//     ? '/asprtech.gitlab.io/'
//     : '/'
// }