import {ElementTypes} from '@/helpers/constants'
import {SHUKNSdataModel} from '@/helpers/dataTypesForProducts'
import axios from 'axios'
import createValues from '../src/helpers/createValuesForProduct'


export const productState = {
  state: {
    shuknsParams: [
      {
        paramId: 1,
        title: 'Название электрощита',
        // value: 'Мой электрощит',
        value: [{
          value: 'Мой электрощит',
          title: 'Мой электрощит',
          value_id: 0
        }],
        label: 'Введите название',
        elementType: ElementTypes.input,
        fieldValueName: SHUKNSdataModel.name.name,
        fieldValueType: SHUKNSdataModel.name.dataType,
        valueForEdit: '',
      },
      {
        paramId: 2,
        title: '* Укажите мощность насосов, кВт',
        value: createValues([0.75, 1.5, 2.2, 3, 4, 5.5, 7.5, 11, 15, 18, 22, 30, 37, 45, 55, 75, 90]),
        elementType: ElementTypes.select,
        label: 'Мощность насосов',
        fieldValueName: SHUKNSdataModel.save_json.values.power.name,
        fieldValueType: SHUKNSdataModel.save_json.values.power.dataType,
        valueForEdit: '',
      },
      {
        paramId: 3,
        title: '* Укажите количество насосов',
        value: createValues([1, 2, 3]),
        label: 'Количество насосов',
        elementType: ElementTypes.select,
        fieldValueName: SHUKNSdataModel.save_json.values.pumpsCount.name,
        fieldValueType: SHUKNSdataModel.save_json.values.pumpsCount.dataType,
        valueForEdit: '',
      },
      {
        paramId: 4,
        title: '* Укажите тип пуска насосов',
        value: createValues(['Прямой пуск', 'Плавный пуск', 'Преобразователь частоты']),
        label: 'Тип пуска',
        elementType: ElementTypes.select,
        fieldValueName: SHUKNSdataModel.save_json.values.starter.name,
        fieldValueType: SHUKNSdataModel.save_json.values.starter.dataType,
        valueForEdit: '',

      },
      {
        paramId: 5,
        title: '* Укажите количество вводов',
        value: createValues([1, 2]),
        label: 'Количество вводов',
        elementType: ElementTypes.select,
        fieldValueName: SHUKNSdataModel.save_json.values.inputCount.name,
        fieldValueType: SHUKNSdataModel.save_json.values.inputCount.dataType,
        valueForEdit: '',
      },
      {
        paramId: 6,
        title: '* Укажите степень защиты электрощита',
        value:  createValues(['Уличное (УХЛ1)', 'В помещении (УХЛ4)']),
        label: 'Cтепень защиты электрощита',
        elementType: ElementTypes.select,
        fieldValueName: SHUKNSdataModel.save_json.values.ip.name,
        fieldValueType: SHUKNSdataModel.save_json.values.ip.dataType,
        valueForEdit: '',
      },
      {
        paramId: 7,
        title: '* Укажите количество подключаемых задвижек',
        value: createValues([0, 1, 2, 3, 4]),
        label: 'Количество подключаемых задвижек',
        elementType: ElementTypes.select,
        fieldValueName: SHUKNSdataModel.save_json.values.zdv.name,
        fieldValueType: SHUKNSdataModel.save_json.values.zdv.dataType,
        valueForEdit: '',
      },
      {
        paramId: 8,
        title: '* Укажите производителя основных комплектующих',
        value: createValues(['Schneider Electric','Бюджетный вар.(IEK, УПП и ПЧ пр-ва ESQ)']),
        label: 'Производитель',
        elementType: ElementTypes.select,
        fieldValueName: SHUKNSdataModel.save_json.values.producer.name,
        fieldValueType: SHUKNSdataModel.save_json.values.producer.dataType,
        valueForEdit: '',
      },
      {
        paramId: 9,
        title: '* GSM модем',
        value: createValues(['Да','Нет']),
        label: 'Добавить GSM модем?',
        elementType: ElementTypes.select,
        fieldValueName: SHUKNSdataModel.save_json.values.gsm.name,
        fieldValueType: SHUKNSdataModel.save_json.values.gsm.dataType,
        valueForEdit: '',
      },
    ],
    productSpecification: {

    }
  },
  mutations: {
    saveProductSpecification(state, data) {
      state.productSpecification = JSON.parse(data)
    },
    clearProductSpecification(state){
      state.productSpecification = {}
    }
  },
  actions: {
    calcApp({ commit }, data) {
      axios.post(`/calcapp/${data.type}`, data).then(res => {
        let result = JSON.stringify(res.data);
        commit('saveProductSpecification', result);
      }).catch(err => {
      });
    }
  },
  getters: {}
}